/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_01;

import javax.swing.JOptionPane;
import java.util.Random;
/**
 *
 * @author PC
 */
public class Logic {
    
    private Entities[][] asientos;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";

    public Logic() {
        asientos = new Entities[8][9];
    }

    /**
     * Rellena la matriz de asientos poniendo una letra y un número en cada
     * espacio, además asigna el precio según la lógica determinada
     */
    public void generarAsientos() {
        Random rand = new Random();
        int num = 25;
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                int precio = f < 3 ? 2000 : f < 6 ? 3000 : 4000;
                asientos[f][c] = new Entities(String.valueOf((char) (f + 65)), c + 1, precio);
                
            }
        }
    }

    /**
     * Crea un string que es la representación de los asientos, cambia el color
     * dependiendo del estado de los mismos.
     *
     * @return String con los asientos concatenados.
     */
    public String verAsientos() {
        String str = "";
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (!asientos[f][c].isEstado()) {
                    str += ANSI_GREEN;
                } else {
                    str += ANSI_BLUE;
                }
                str += asientos[f][c].getNombre() + " ";
                str += ANSI_RESET;
            }
            str += "\n";
        }
        return str;
    }
    public boolean venderAsiento(String numero) {
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (numero.equalsIgnoreCase(asientos[f][c].getNombre())) {
                    if (!asientos[f][c].isEstado()) {
                        asientos[f][c].setEstado(true);
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }
    

}
